<?php
class Database 
{
    private $host = '212.8.250.53';
    private $db = 'abz';
    private $user = 'tester';
    private $pass = 'tester';
    private $conn;

    public function __construct() 
    {
        try {
            $this->conn = new PDO("pgsql:host={$this->host};dbname={$this->db}", $this->user, $this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } 
        catch (PDOException $e) 
        {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    public function __destruct() 
    {
        $this->conn = null;
    }

    public function getLeads() 
    {
        try 
        {
            $stmt = $this->conn->prepare("SELECT * FROM leads");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } 
        catch (PDOException $e) 
        {
            echo 'Query failed: ' . $e->getMessage();
            return [];
        }
    }

    public function addLead($name, $email) 
    {
        try 
        {
            $stmt = $this->conn->prepare("INSERT INTO leads (name, email) VALUES (:name, :email)");
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            return $stmt->execute();
        } 
        catch (PDOException $e) 
        {
            echo 'Insert failed: ' . $e->getMessage();
            return false;
        }
    }
}
?>