<?php

class addlead
{
    private $message;
    private $body;
    private $status;

    public function resp($message = null, $body = null, $status = true)
    {
        $responseData = []; // Define the response array
    
        $this->message = $message;
        $this->body = $body;
        $this->status = $status;

        if($this->message)
        {
            $responseData["message"] = $this->message; // Corrected typo
        }
        if($this->body)
        {
            $responseData["data"] = $this->body; // Corrected typo
        }

        header('Content-Type: application/json');
        http_response_code($status ? 200 : 400); // Use status to determine HTTP code
        echo json_encode($responseData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        exit();
    }
}

?>