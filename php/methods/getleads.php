<?php

class getleads
{
    private $data;
    private $response;

    public function __construct($data, $responseOject) 
    {
        $this->data = $data;
        $this->response = $responseOject;
        $this->getLeads();
    }

    public function getLeads()
    {
        $this->response->resp(null, $this->data);
    }
}

?>