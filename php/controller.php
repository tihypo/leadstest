<?php

//FOR DEBUG
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . "/utils/response.php"; // Missing semicolon added
require_once __DIR__ . "/utils/ip.php";
require_once __DIR__ . "/utils/database.php";

$responseObject = new Response();

if ($_SERVER['REQUEST_METHOD'] == 'POST' || $_SERVER['REQUEST_METHOD'] == 'GET')
{
    $databaseObject = new Database();
    $method = isset($_GET['method']) ? strtolower(htmlspecialchars(urldecode($_GET['method']))) : null;
    unset($_GET['method']);
    $data = [];
    $data["queryParams"] =  $_GET;
    $data["postParams"] = $_POST;
    $data["userIp"] = getIp();
    $data["json"] = file_get_contents('php://input') ? json_decode(file_get_contents('php://input'), TRUE) : null;

    if ($method)
    {
        $file_path = __DIR__ . "/methods/" . $method . ".php";
        if (file_exists($file_path)) 
        {
            require_once $file_path;
            new $method($data, $responseObject);
        } 
        else 
        {
            $responseObject->resp("Can't find this method", null);
        }
    } 
    else 
    {
        $responseObject->resp("Bad method", null);
    }
} 
else 
{
    $responseObject->resp("Can't find your method in route", null);
}
?>
